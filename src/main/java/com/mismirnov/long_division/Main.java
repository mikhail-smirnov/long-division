package com.mismirnov.long_division;

import com.mismirnov.long_division.formatter.DivisionFormatter;
import com.mismirnov.long_division.division.Division;
import com.mismirnov.long_division.division.Result;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Input dividend: ");
        int dividend = scanner.nextInt();
        System.out.print("Input divisor: ");
        int divisor = scanner.nextInt();

        Division division = new Division();
        Result result = division.divide(dividend, divisor);
        DivisionFormatter divisionFormatter = new DivisionFormatter();
        String formattedResult = divisionFormatter.format(result);
        System.out.println(formattedResult);

    }
}
