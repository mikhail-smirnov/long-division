package com.mismirnov.long_division.division;

import java.util.ArrayList;
import java.util.List;

import static com.mismirnov.long_division.division.utils.NumberUtils.splitDigits;

public class Division {

    public Result divide(int dividend, int divisor) {

        if (divisor == 0) {
            throw new IllegalArgumentException("Divisor cannot be 0, division by zero");
        }
        dividend = Math.abs(dividend);
        divisor = Math.abs(divisor);

        if (dividend < divisor) {
            throw new IllegalArgumentException("Divisor cannot be greater than dividend");
        }
        List<Integer> digits = splitDigits(dividend);
        int partialDividend = 0;
        int quotient = 0;
        List<DivisionStep> divisionSteps = new ArrayList<>();
        for (int i = 0; i < digits.size(); i++) {
            if (i < 1) {
                partialDividend = digits.get(i);
            } else {
                partialDividend = partialDividend * 10 + digits.get(i);
            }

            if (partialDividend >= divisor) {
                int multiplyResult;
                multiplyResult = partialDividend / divisor * divisor;
                divisionSteps.add(new DivisionStep(partialDividend, multiplyResult));
                quotient = quotient * 10 + partialDividend / divisor;
                partialDividend = partialDividend % divisor;
            } else {
                 {
                    quotient = quotient * 10;
                }
            }
        }
        return new Result(divisionSteps, dividend, divisor, quotient, partialDividend);
    }
}

