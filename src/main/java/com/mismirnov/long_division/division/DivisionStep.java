
package com.mismirnov.long_division.division;

public class DivisionStep {

    private int partialDividend;
    private int multiplyResult;

    public DivisionStep(int partialDividend, int multiplyResult) {
        this.partialDividend = partialDividend;
        this.multiplyResult = multiplyResult;
    }

    public int getPartialDividend() {
        return partialDividend;
    }

    public int getMultiplyResult() {
        return multiplyResult;
    }

}
