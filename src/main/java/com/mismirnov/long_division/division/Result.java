package com.mismirnov.long_division.division;

import java.util.List;

public class Result {

    private List<DivisionStep> divisionSteps;
    private int dividend;
    private int divisor;
    private int quotient;
    private int remainder;

    public Result(List<DivisionStep> result, int dividend, int divisor, int quotient, int remainder) {
        this.divisionSteps = result;
        this.dividend = dividend;
        this.divisor = divisor;
        this.quotient = quotient;
        this.remainder = remainder;
    }

    public int getDividend() {
        return dividend;
    }

    public int getDivisor() {
        return divisor;
    }

    public int getQuotient() {
        return quotient;
    }

    public int getRemainder() {
        return remainder;
    }

    public List<DivisionStep> getDivisionSteps() {
        return divisionSteps;
    }

}