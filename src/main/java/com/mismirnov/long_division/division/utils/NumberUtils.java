package com.mismirnov.long_division.division.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NumberUtils {

    public static List<Integer> splitDigits(int number) {
        List<Integer> digits = new ArrayList<>();
        int modifiedNumber = number;
        while (modifiedNumber > 0) {
            digits.add(modifiedNumber % 10);
            modifiedNumber = modifiedNumber / 10;
        }
        Collections.reverse(digits);
        return digits;
    }
}
