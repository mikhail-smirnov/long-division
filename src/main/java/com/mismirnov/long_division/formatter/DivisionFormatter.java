package com.mismirnov.long_division.formatter;

import com.mismirnov.long_division.division.DivisionStep;
import com.mismirnov.long_division.division.Result;
import java.util.List;

import static java.lang.System.lineSeparator;

public class DivisionFormatter {

    public String format(Result result) {
        return formatRemainder(result, formatSteps(result, formatHeader(result))).toString();
    }

    private StringBuilder formatHeader(Result result){
        List<DivisionStep> divisionSteps = result.getDivisionSteps();
        StringBuilder formattedHeader = new StringBuilder();
        String dividend = String.valueOf(result.getDividend());
        String divisor = String.valueOf(result.getDivisor());
        String quotient = String.valueOf(result.getQuotient());
        String firstMultiplyResult = String.valueOf(divisionSteps.get(0).getMultiplyResult());
        formattedHeader.append("_" + dividend + "│" + divisor);
        formattedHeader.append(lineSeparator());
        formattedHeader.append(" " + firstMultiplyResult).append(repeatChar( ' ', dividend.length() - firstMultiplyResult.length()) + "│" + repeatChar('-', quotient.length()));
        formattedHeader.append(lineSeparator());
        formattedHeader.append(" " + String.format(repeatChar( '-', firstMultiplyResult.length()))).append(repeatChar(' ', dividend.length() - firstMultiplyResult.length()) + "│" + quotient);
        formattedHeader.append(lineSeparator());
        return formattedHeader;
    }

    private StringBuilder formatSteps(Result result, StringBuilder formattedHeader) {
        List<DivisionStep> divisionSteps = result.getDivisionSteps();
        StringBuilder formattedSteps = formattedHeader;
        int numberPosition = setNumberPosition(result);
        for (int i = 1; i < divisionSteps.size(); i++) {
            formattedSteps.append(String.format("%" + (numberPosition) + "s", ("_" + divisionSteps.get(i).getPartialDividend())));
            formattedSteps.append(lineSeparator());
            formattedSteps.append(String.format("%" + (numberPosition) + "s", divisionSteps.get(i).getMultiplyResult()));
            formattedSteps.append(lineSeparator());
            formattedSteps.append(String.format("%" + (numberPosition) + "s", repeatChar('-', String.valueOf(divisionSteps.get(i).getMultiplyResult()).length())));
            formattedSteps.append(lineSeparator());
            numberPosition++;
        }

        return formattedSteps;
    }

    private StringBuilder formatRemainder(Result result, StringBuilder formattedBody) {
        List<DivisionStep> divisionSteps = result.getDivisionSteps();
        StringBuilder formattedRemainder = formattedBody;
        String remainder = String.valueOf(result.getRemainder());
        int numberPosition = setNumberPosition(result);
        for (int i = 1; i < divisionSteps.size() - 1; i++) {
            if (i != divisionSteps.size() - 1) {
                numberPosition++;
            }
        }

        formattedRemainder.append(String.format("%" + (numberPosition) + "s", remainder));
        formattedRemainder.append(lineSeparator());

        return formattedRemainder;
    }

    private String repeatChar(char character, int times) {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < times; i++) {
            string.append(character);
        }
        return string.toString();
    }

    private int setNumberPosition(Result result) {
        int numberPosition;
        List<DivisionStep> divisionSteps = result.getDivisionSteps();
        int remainder = result.getRemainder();
        int firstMultiplyResult = divisionSteps.get(0).getMultiplyResult();
        int firstPartialDividend = divisionSteps.get(0).getPartialDividend();
        if ((firstPartialDividend == firstMultiplyResult) && (remainder != 0)) {
            numberPosition = String.valueOf(firstMultiplyResult).length() + 2;
        } else if (divisionSteps.size() == 1) {
            numberPosition = String.valueOf(firstMultiplyResult).length() + 1;
        } else if ((String.valueOf(divisionSteps.get(1).getMultiplyResult()).length() - String.valueOf(firstMultiplyResult).length()) > 0) {
            numberPosition = String.valueOf(divisionSteps.get(1).getMultiplyResult()).length() + 1;
        } else {
            numberPosition = String.valueOf(firstMultiplyResult).length() + 1;
        }

        return numberPosition;
    }
}

