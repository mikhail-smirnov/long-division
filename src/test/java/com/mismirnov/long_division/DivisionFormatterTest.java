package com.mismirnov.long_division;

import com.mismirnov.long_division.division.DivisionStep;
import com.mismirnov.long_division.division.Result;
import com.mismirnov.long_division.formatter.DivisionFormatter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.lineSeparator;
import static org.junit.Assert.*;

public class DivisionFormatterTest {

    @Test
    public void givenResultContainsLargeNumber_whenFormat_thenLongFormattedResultReturned() {
        List<DivisionStep> divisionSteps = new ArrayList<>(Arrays.asList(new DivisionStep(676, 355), new DivisionStep(3217, 3195), new DivisionStep(2276, 2130), new DivisionStep(1464, 1420)));
        Result result = new Result(divisionSteps, 6767764, 355, 19064,44);
        DivisionFormatter divisionFormatter = new DivisionFormatter();
        String output = divisionFormatter.format(result);
        StringBuilder expectedOutput = new StringBuilder();
        expectedOutput.append("_6767764│355").append(lineSeparator());
        expectedOutput.append(" 355    │-----").append(lineSeparator());
        expectedOutput.append(" ---    │19064").append(lineSeparator());
        expectedOutput.append("_3217").append(lineSeparator());
        expectedOutput.append(" 3195").append(lineSeparator());
        expectedOutput.append(" ----").append(lineSeparator());
        expectedOutput.append(" _2276").append(lineSeparator());
        expectedOutput.append("  2130").append(lineSeparator());
        expectedOutput.append("  ----").append(lineSeparator());
        expectedOutput.append("  _1464").append(lineSeparator());
        expectedOutput.append("   1420").append(lineSeparator());
        expectedOutput.append("   ----").append(lineSeparator());
        expectedOutput.append("     44").append(lineSeparator());
        assertEquals(output, expectedOutput.toString());

    }

    @Test
    public void givenResultContainsDivisorEqualsDividend_whenFormat_thenShortFormattedResultReturned() {
        List<DivisionStep> divisionSteps = new ArrayList<>(Arrays.asList(new DivisionStep(103, 103)));
        Result result = new Result(divisionSteps, 103, 103, 1, 0);
        DivisionFormatter divisionFormatter = new DivisionFormatter();
        String output = divisionFormatter.format(result);
        StringBuilder expectedOutput = new StringBuilder();
        expectedOutput.append("_103│103").append(lineSeparator());
        expectedOutput.append(" 103│-").append(lineSeparator());
        expectedOutput.append(" ---│1").append(lineSeparator());
        expectedOutput.append("   0").append(lineSeparator());
        assertEquals(output, expectedOutput.toString());

    }
}