package com.mismirnov.long_division;

import com.mismirnov.long_division.division.Division;
import com.mismirnov.long_division.division.DivisionStep;
import com.mismirnov.long_division.division.Result;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DivisionTest {

    @Test
    public void givenLargeNumber_whenDivisionTest_thenRemainderReturned() {
        Division division = new Division();
        int dividend = 6767764;
        int divisor = 355;
        int quotient = 19064;
        int remainder = 44;
        List<DivisionStep> divisionSteps = new ArrayList<>(Arrays.asList(new DivisionStep(676, 355), new DivisionStep(3217, 3195), new DivisionStep(2276, 2130), new DivisionStep(1464, 1420)));
        Result expectedOutput = new Result(divisionSteps,dividend, divisor, quotient, remainder);
        Result result = division.divide(dividend, divisor);
        assertEquals(result.getQuotient(), expectedOutput.getQuotient());
    }

    @Test
    public void givenDivisorEqualsDividend_whenDivisionTest_thenCorrectRemainderEqualsZeroReturned() {
        Division division = new Division();
        int dividend = 103;
        int divisor = 103;
        int quotient = 1;
        int remainder = 0;
        List<DivisionStep> divisionSteps = new ArrayList<>(Arrays.asList(new DivisionStep(103, 103)));
        Result expectedOutput = new Result(divisionSteps, dividend, divisor, quotient, remainder);
        Result result = division.divide(dividend, divisor);
        assertEquals(result.getQuotient(), expectedOutput.getQuotient());
    }

    @Test
    public void givenLargeNumberWithZeros_whenDivisionTest_thenCorrectRemainderReturned() {
        Division division = new Division();
        int dividend = 34000001;
        int divisor = 32;
        int quotient = 1062500;
        int remainder = 1;
        List<DivisionStep> divisionSteps = new ArrayList<>(Arrays.asList(new DivisionStep(34, 32), new DivisionStep(200, 192), new DivisionStep(160, 160)));        Result expectedOutput = new Result(divisionSteps,dividend, divisor, quotient, remainder);
        Result result = division.divide(dividend, divisor);
        assertEquals(result.getQuotient(), expectedOutput.getQuotient());
    }
}
