package com.mismirnov.long_division;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mismirnov.long_division.division.utils.NumberUtils.splitDigits;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;


public class NumberUtilsTest {

    @Test
    public void givenLargeIntNumber_whenSplitDigits_thenNumberDigitsReturned() {
        int dividend = 6767764;
        List<Integer> expectedOutput = new ArrayList<>(Arrays.asList(6, 7, 6, 7, 7, 6, 4));
        List<Integer> result = splitDigits(dividend);
        assertReflectionEquals(result, expectedOutput);

    }

    @Test
    public void givenOneNumber_whenSplitDigits_thenOneDigitReturned() {
        int dividend = 1;
        List<Integer> expectedOutput = new ArrayList<>(Arrays.asList(1));
        List<Integer> result = splitDigits(dividend);
        assertReflectionEquals(result, expectedOutput);

    }

}
